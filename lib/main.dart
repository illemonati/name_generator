import 'package:flutter/material.dart';
import 'package:english_words/english_words.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'Name Generator',
        home: const RandomWords(),
        theme: ThemeData(
            appBarTheme: const AppBarTheme(
                backgroundColor: Color.fromARGB(255, 0, 8, 255),
                foregroundColor: Colors.black)));
  }
}

class RandomWords extends StatefulWidget {
  const RandomWords({Key? key}) : super(key: key);

  @override
  State<RandomWords> createState() => _RandomWordsState();
}

class _RandomWordsState extends State<RandomWords> {
  final _suggestions = <WordPair>[];
  final _biggerFont = const TextStyle(fontSize: 18);
  final _saved = <WordPair>{};

  void _pushSaved() {
    Navigator.of(context).push(MaterialPageRoute<void>(builder: (context) {
      final tiles = _saved.map((wordPair) => ListTile(
              title: Text(
            wordPair.asPascalCase,
            style: _biggerFont,
          )));
      final divided = tiles.isNotEmpty
          ? ListTile.divideTiles(
              context: context,
              tiles: tiles,
            ).toList()
          : <Widget>[];

      return Scaffold(
          appBar: AppBar(title: const Text('Saved Suggestions')),
          body: ListView(
            children: divided,
          ));
    }));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(title: const Text('Generated Names'), actions: [
          IconButton(
            onPressed: _pushSaved,
            icon: const Icon(Icons.list),
            tooltip: 'Saved Suggestions',
          )
        ]),
        body: ListView.builder(
          padding: const EdgeInsets.all(16.0),
          itemBuilder: (context, i) {
            if (i.isOdd) return const Divider();

            final index = i ~/ 2;

            if (index >= _suggestions.length) {
              _suggestions.addAll(generateWordPairs().take(10));
            }

            final suggestion = _suggestions[index];

            final entrySaved = _saved.contains(suggestion);

            return ListTile(
                title: Text(suggestion.asPascalCase, style: _biggerFont),
                trailing: Icon(
                  entrySaved ? Icons.favorite : Icons.favorite_border,
                  color: entrySaved ? Colors.red : null,
                  semanticLabel: entrySaved ? 'Unsave' : 'Save',
                ),
                onTap: () {
                  setState(() {
                    if (entrySaved) {
                      _saved.remove(suggestion);
                    } else {
                      _saved.add(suggestion);
                    }
                  });
                });
          },
        ));
  }
}
